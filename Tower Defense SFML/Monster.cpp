#include"Monster.h"
Monster::Monster()
{
	this->attack = 0;
	this->life = 0;
	this->speed = 0;
}
Monster::Monster(int life, int attack, int speed)
{
	this->life = life;
	this->attack = attack;
	this->speed = speed;
}
int Monster::GetAttack()const
{
	return this->attack;
}
void Monster::Setlife(int Life)
{
	this->life = Life;
}
void Monster::Setattack(int attack)
{
	this->attack = attack;
}
int Monster::GetSpeed() const
{
	return this->speed;
}
void Monster::SetSpeed(int speed)
{
	this->speed = speed;
}
int Monster::GetLife()const
{
	return this->life;
}