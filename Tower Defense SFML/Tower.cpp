#include "Tower.h"

Tower::Tower()
{
	this->attackSpeed = 0;
	this->damage = 0;
	this->range = 0;
}

Tower::Tower(float range, float damage, float attackSpeed)
{
	this->attackSpeed = attackSpeed;
	this->damage = damage;
	this->range = range;
}

float Tower::getRange() const
{
	return this->range;
}

float Tower::getDamage() const
{
	return this->damage;
}

float Tower::getAttackSpeed() const
{
	return this->attackSpeed;
}

void Tower::setRange(float range)
{
	this->range = range;
}

void Tower::setDamage(float damage)
{
	this->damage = damage;
}

void Tower::setAttackSpeed(float attackSpeed)
{
	this->attackSpeed = attackSpeed;
}
