#pragma once
class Tower {
private:
	float range;
	float damage;
	float attackSpeed;
public:
	Tower();
	Tower(float, float, float);
	float getRange() const;
	float getDamage() const;
	float getAttackSpeed() const;
	void setRange(float);
	void setDamage(float);
	void setAttackSpeed(float);
}; 
