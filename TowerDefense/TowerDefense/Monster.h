#pragma once
class Monster
{
private:
	int life;
	int attack;
	int speed;
public:
	Monster();
	Monster(int,int,int);
	int GetLife() const;
	int GetAttack() const;
	void Setlife(int);
	void Setattack(int);
	int GetSpeed() const;
	void SetSpeed(int);
};
